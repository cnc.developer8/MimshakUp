$(document).ready(function () {
    var owl = $(".slider");
    owl.owlCarousel({
        center: false,
        nav: true,
        items: 7,
        afterAction: function () {
            if (this.itemsAmount > this.visibleItems.length) {
                $('.next').show();
                $('.prev').show();

                $('.next').removeClass('disabled');
                $('.prev').removeClass('disabled');
                if (this.currentItem == 0) {
                    $('.prev').addClass('disabled');
                }
                if (this.currentItem == this.maximumItem) {
                    $('.next').addClass('disabled');
                }

            } else {
                $('.next').hide();
                $('.prev').hide();
            }
        },
        singleItem: true,
        rtl: true,
        loop: false,
        dots: false,
        autoWidth: true,
        slideTransition: 'linear',

    });
    var get_current_index = $('.owl-carousel.owl-rtl .owl-item a.current').parent().index() - 1;
    owl.trigger('to.owl.carousel', [get_current_index, 0])


    $(document).click(function () {
        $(".more_list").hide();
    });
    $('.more1 .moreanchor').click(function (e) {
        if ($(this).hasClass('open')) {
            $(this).removeClass('open');
            $(this).siblings('.more_list').hide()
        }
        else {
            $('.more1 .moreanchor').removeClass('open');
            $(this).addClass("open");
            $('.more_list').hide()
            $(this).siblings('.more_list').toggle();
        }
        e.stopPropagation();
    });
    $('.more a').click(function (e) {
        if ($(this).hasClass('open')) {
            $(this).removeClass('open');
            $(this).siblings('.more_list').hide()
        }
        else {
            $('.more a').removeClass('open');
            $(this).addClass("open");
            $('.more_list').hide()
            $(this).siblings('.more_list').toggle();
        }
        e.stopPropagation();
    });

    $('.edit_list .edit').click(function (e) {
        $(this).siblings('.more_list').toggle()
        e.stopPropagation();
        $(".side_anc .more_list").hide();
    });
    $(function () {
        $(".sortable").sortable();
        $(".sortable").disableSelection();
    });
    $(window).scroll(function () {
        if ($(window).scrollTop() >= 40) {
            $('header').addClass('fixed_header');
        }
        else {
            $('header').removeClass('fixed_header');
        }
    });
    $(".movesA").hide();
    $(".movesa").show();
    $(".tab li a").click(function () {
        var type = $(this).data("type");
        $(".movesA").hide();
        $(".moves" + type).show();
        $(".tab li a").removeClass("active");
        $(this).addClass("active");
        $(".moves" + type).find(".accord_body:first").addClass('tabbody');
        $(".tabbody").show();
        $(".slider").trigger('refresh.owl.carousel');
        owl.trigger('to.owl.carousel', [get_current_index, 0]);
    });

    $('.slider a').click(function () {
        $('.body_open .slider a').removeClass("current");
        $(this).addClass("current");
    });
    $(".body_open .stepA").hide();
    $(".body_open .step1").show();
    $(".body_open .slider a").click(function () {
        var type = $(this).data("type");
        $(".stepA").hide();
        $(".step" + type).show();
        $(".body_open .slider a").removeClass("current");
        $(this).addClass("current");
    });
    $(".status a").click(function () {
        $(this).next(".status_list").toggle();
    });
    $(".status_list li").click(function () {
        var listval = $(this).text();
        $(this).parent(".status_list").prev(".statustext").children(".statuswrite").text(listval);
        $(".status_list").slideUp();
    });

    $('.accord_head ').click(function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).next('.accord_body').slideUp();
            $(".slider").trigger('refresh.owl.carousel');
            $(".slider").trigger('to.owl.carousel', [3, 0])
            $(this).removeClass('accord_open');
            $(this).next('.accord_body').removeClass("body_open");
        }
        else if ($('.accord_body').hasClass("tabbody")) {
            $('.accord_body').hide();
            $('.accord_body').removeClass("tabbody");
        }
        else {
            $('.open_arrow').removeClass('active');
            $(".accord_head").removeClass('accord_open');
            $(".accord_body").removeClass('body_open');
            $(this).addClass('active');
            $(this).addClass('accord_open');
            $('.accord_body').slideUp();
            $(this).next('.accord_body').addClass("body_open");
            $(this).next('.accord_body').slideToggle();
            $(".slider").trigger('refresh.owl.carousel');
            owl.trigger('to.owl.carousel', [get_current_index, 0]);
        }
        $(".body_open .stepA").hide();
        $(".body_open .step1").show();
        $(".body_open .slider a").click(function () {
            var type = $(this).data("type");
            $(".stepA").hide();
            $(".step" + type).fadeIn();
            $(".body_open .slider a").removeClass("current");
            $(this).addClass("current");
        });
    });

    // catalogpage
    $('.acc_list span').click(function () {
        if ($(this).parent().hasClass('active')) {
            $(this).next('.acc_drop').hide();
            $(this).parent().removeClass('active');
        }
        else {
            $(this).parent().toggleClass('active')
            $(this).next('.acc_drop').toggle();
        }
    });
    $(document).click(function () {
        $(".first_drop").hide();
    });
    $('.catalog_more').click(function (e) {
        $('.first_drop').hide();
        e.stopPropagation();
        $(this).next('.first_drop').toggle();
    })
    $('.heart').click(function () {
        if ($(this).hasClass('heartred')) {
            $(this).removeClass('heartred');
        }
        else {
            $(this).addClass('heartred');
        }
    });
    $('.search input').keyup(function () {
        if ($(this).val().length > 0) {
            $(this).parent('.search').addClass('warning');
            $(this).next('img').attr('src', 'image/closegrey.svg')
            $(this).next('img').addClass('closetext')
        }
        else {
            $(this).parent('.search').removeClass('warning');
            $(this).next('img').attr('src', 'image/search.svg')
            $(this).next('img').removeClass('closetext')
        }
    });
    $(document).on('click', '.closetext', function(){
        $(this).prev().val("")
        $(this).attr('src', 'image/search.svg').removeClass('closetext');
        $(this).parent('.search').removeClass('warning');
});
    $('.checkdiv input').click(function () {
        if ($('.checkdiv input').is(':checked')) {
            $(this).parent().prev().addClass('change_text').children('p').text('בחרו שם לנושא אליו תרצו להוסיף את היחידה')
        }
    });
    $('.sending_mail').click(function(){
        $(this).hide();
        $(this).next('.green_btn').show();
    });
    $('.puzzle p img').click(function(){
        $('.puzzle').hide();
    });

    var owl2 = $(".myclass_slider");
    owl2.owlCarousel({
        center: false,
        nav: true,
        items: 7,
        afterAction: function () {
            if (this.itemsAmount > this.visibleItems.length) {
                $('.next').show();
                $('.prev').show();

                $('.next').removeClass('disabled');
                $('.prev').removeClass('disabled');
                if (this.currentItem == 0) {
                    $('.prev').addClass('disabled');
                }
                if (this.currentItem == this.maximumItem) {
                    $('.next').addClass('disabled');
                }

            } else {
                $('.next').hide();
                $('.prev').hide();
            }
        },
        singleItem: true,
        rtl: true,
        loop: false,
        dots: false,
        autoWidth: true,
        slideTransition: 'linear',

    });
    var owl3 = $(".catalog_slider_in");
    owl3.owlCarousel({
        center: false,
        nav: true,
        items: 6,
        afterAction: function () {
            if (this.itemsAmount > this.visibleItems.length) {
                $('.next').show();
                $('.prev').show();

                $('.next').removeClass('disabled');
                $('.prev').removeClass('disabled');
                if (this.currentItem == 0) {
                    $('.prev').addClass('disabled');
                }
                if (this.currentItem == this.maximumItem) {
                    $('.next').addClass('disabled');
                }

            } else {
                $('.next').hide();
                $('.prev').hide();
            }
        },
        singleItem: true,
        rtl: true,
        loop: false,
        dots: false,
        autoWidth: true,
        slideTransition: 'linear',

    });

    $('.copy_link').click(function(){
        $(this).next('.green_btn').show();
    });
    
    $('.linkclick').click(function(){
        $('.addlink').show();
        $('.addlink2').show();
        $('.main_modal').hide();
        $('.main_modal2').hide();
    });
    $('.fileclick').click(function(){
        $('.addfile').show();
        $('.addfile2').show();
        $('.main_modal').hide();
        $('.main_modal2').hide();
    });
    $('.forwardarrow').click(function(){
        $('.main_modal').show();
        $('.main_modal2').show();
        $(this).parent().hide();
    });

    $(".setA").hide();
    $(".setx").show();
    $(".tab1 a").click(function () {
        var type = $(this).data("type");
        $(".setA").hide();
        $(".set" + type).fadeIn();
        $(".tab1 a").removeClass("active");
        $(this).addClass("active");
    });

    $('.setacc_head').click(function(){
        if($(this).hasClass('active')){
            $(this).removeClass('active');
            $(this).next('.setacc_body').slideUp(); 
        }
        else{
            $('.setacc_head').removeClass('active');
            $(this).addClass('active');
            $('.setacc_body').slideUp(); 
            $(this).next('.setacc_body').slideToggle();
        }
    });
    $('.studentgroup li img').click(function(){
        $(this).parent('li').remove();
    });
    $('.student_list li').click(function(){
        $(this).toggleClass('active')
    });
   
});
